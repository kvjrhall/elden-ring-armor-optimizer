# Live Demo

The [interactive live demo](https://kvjrhall.gitlab.io/elden-ring-armor-optimizer/)
is automatically built from `main` each time it is updated, so feel free to
visit that link when you want to use the latest version.

# Why this project?

There aren't really a lot of Elden Ring armor optimization projects that are
moving very quickly, so I figured that I'd get something started and see if
there is any interest. It is also pretty handy to have a working example of
prerendering an angular application automatically and publishing the output to
its GitLab project. Hopefully this can also serve as a skeleton for other
people's projects, as well.
