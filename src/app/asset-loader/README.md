The type `DOMParser` is available through Mozilla's webkit in all browsers,
but server-side rendering occurs in the context of `node.js` during
`angular-prerender`. Consequently, we cannot just parse an HTML document and
expect that we get results.

According to [this (probably outdated) blog post](https://www.willtaylor.blog/angular-universal-gotchas/),
Angular constructs its SSR `Document` by invoking
[`domino`](https://www.npmjs.com/package/domino). Sure enough, if we
intentionally use `domino` for parsing during server-side rendering, the
rendering and dom manipulations that we want go according to plan.
