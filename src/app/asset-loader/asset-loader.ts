import { InjectionToken } from '@angular/core';
import { Observable } from 'rxjs';

export interface AssetLoader {
  getAsset<T>(relative_path: string): Observable<T>;
}

export const ASSET_LOADER = new InjectionToken<AssetLoader>('ASSET_LOADER');
