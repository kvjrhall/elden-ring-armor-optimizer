import { TestBed } from '@angular/core/testing';
import { of } from 'rxjs';
import { AssetLoader, ASSET_LOADER } from '../asset-loader/asset-loader';
import { ArmorSets, OptimizerService } from './optimizer.service';

describe('OptimizerService', () => {
  let armorSets: ArmorSets;
  let service: OptimizerService;
  let loader: AssetLoader;

  beforeEach(() => {
    armorSets = new Map();
    loader = {
      getAsset: jasmine
        .createSpy('getAsset')
        .withArgs('armor.json')
        .and.returnValue(of(armorSets)),
    };
    loader.getAsset;

    TestBed.configureTestingModule({
      providers: [{ provide: ASSET_LOADER, useValue: loader }],
    });
    service = TestBed.inject(OptimizerService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
