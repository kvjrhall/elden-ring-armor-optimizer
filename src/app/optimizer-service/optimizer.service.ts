import { Inject, Injectable } from '@angular/core';
import { map, Observable, ReplaySubject } from 'rxjs';
import { AssetLoader, ASSET_LOADER } from '../asset-loader/asset-loader';

export interface Item {
  fire: number;
  holy: number;
  kind: 'Chest Armor' | 'Gauntlets' | 'Helm' | 'Leg Armor';
  light: number;
  magic: number;
  name: string;
  phy: number;
  'phy.pierce': number;
  'phy.slash': number;
  'phy.strike': number;
  poise: number;
  'res.focus': number;
  'res.immunity': number;
  'res.robustness': number;
  'res.vitality': number;
  weight: number;
}

export interface ArmorSet {
  'Chest Armor'?: Item;
  Gauntlets?: Item;
  Helm?: Item;
  'Leg Armor'?: Item;
}

export type ArmorSets = Map<String, ArmorSet>;

@Injectable({
  providedIn: 'root',
})
export class OptimizerService {
  private _armorSets$ = new ReplaySubject<ArmorSets>();

  constructor(@Inject(ASSET_LOADER) private readonly assetLoader: AssetLoader) {
    this.assetLoader
      .getAsset<Object>('armor.json')
      .pipe(
        map((d) => new Map<string, ArmorSet>(Object.entries(d))),
        map((m) => newSortedMap(m))
      )
      .subscribe(this._armorSets$);
  }

  get armorSets$(): Observable<ArmorSets> {
    return this._armorSets$;
  }
}

function newSortedMap<V>(m: Map<string, V>): Map<string, V> {
  return new Map([...m].sort((a, b) => String(a[0]).localeCompare(b[0])));
}
