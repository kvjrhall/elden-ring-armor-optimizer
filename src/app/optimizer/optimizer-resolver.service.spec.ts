import { TestBed } from '@angular/core/testing';
import { OptimizerService } from '../optimizer-service/optimizer.service';
import { OptimizerResolverService } from './optimizer-resolver.service';

describe('OptimizerResolverService', () => {
  let resolver: OptimizerResolverService;
  let service: OptimizerService;

  beforeEach(() => {
    service = jasmine.createSpyObj('OptimizerService', ['getJson']);
    TestBed.configureTestingModule({
      providers: [{ provide: OptimizerService, useValue: service }],
    });
    resolver = TestBed.inject(OptimizerResolverService);
  });

  it('should be created', () => {
    expect(resolver).toBeTruthy();
  });
});
