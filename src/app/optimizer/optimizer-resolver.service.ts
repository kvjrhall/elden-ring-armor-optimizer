import { Injectable } from '@angular/core';
import {
  ActivatedRouteSnapshot,
  Resolve,
  RouterStateSnapshot,
} from '@angular/router';
import { Observable, take } from 'rxjs';
import {
  ArmorSets,
  OptimizerService,
} from '../optimizer-service/optimizer.service';

/**
 * Prerendering should [1] wait for data before rendering view, which we
 * accomplish by [2] using a view resolver to fetch the data before view
 * rendering begins.
 *
 * https://stackoverflow.com/questions/49504266/wait-for-data-before-rendering-view-in-angular-5
 * https://angular.io/guide/router-tutorial-toh#fetch-data-before-navigating
 */
@Injectable({
  providedIn: 'root',
})
export class OptimizerResolverService implements Resolve<ArmorSets> {
  constructor(private readonly demoService: OptimizerService) {}

  resolve(
    route: ActivatedRouteSnapshot,
    state: RouterStateSnapshot
  ): Observable<ArmorSets> {
    console.log('OptimizerResolverService', 'resolve');
    return this.demoService.armorSets$.pipe(take(1));
  }
}
