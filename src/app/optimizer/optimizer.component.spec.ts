import { ComponentFixture, TestBed } from '@angular/core/testing';
import { ActivatedRoute } from '@angular/router';
import { ActivatedRouteStub } from 'src/testing/activated-route-stub';
import { OptimizerComponent } from './optimizer.component';

describe('OptimizerComponent', () => {
    let component: OptimizerComponent;
    let fixture: ComponentFixture<OptimizerComponent>;

    beforeEach(async () => {
        await TestBed.configureTestingModule({
            declarations: [OptimizerComponent],
            providers: [{ provide: ActivatedRoute, useClass: ActivatedRouteStub }],
        }).compileComponents();
    });

    beforeEach(() => {
        fixture = TestBed.createComponent(OptimizerComponent);
        component = fixture.componentInstance;
        fixture.detectChanges();
    });

    it('should create', () => {
        expect(component).toBeTruthy();
    });
});
