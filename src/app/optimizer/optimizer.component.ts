import { isPlatformBrowser } from '@angular/common';
import {
  AfterViewInit,
  Component,
  ElementRef,
  Inject,
  OnInit,
  PLATFORM_ID,
  ViewChild,
} from '@angular/core';
import {
  AbstractControl,
  FormControl,
  FormGroup,
  Validators,
} from '@angular/forms';
import { ActivatedRoute } from '@angular/router';
import { ArmorSet, ArmorSets } from '../optimizer-service/optimizer.service';

type ArmorControlsBase = { [Property in keyof ArmorSet]: FormControl };

class SetControls implements ArmorControlsBase {
  all: FormControl;
  'Chest Armor'?: FormControl;
  Gauntlets?: FormControl;
  Helm?: FormControl;
  'Leg Armor': FormControl;

  constructor() {
    this.all = new FormControl(true);
  }
}

type SetControlType = { [Property in keyof SetControls]: FormControl };

@Component({
  selector: 'app-optimizer',
  templateUrl: './optimizer.component.html',
  styleUrls: ['./optimizer.component.scss'],
})
export class OptimizerComponent implements OnInit, AfterViewInit {
  constructor(
    private readonly route: ActivatedRoute,
    @Inject(PLATFORM_ID) private platformId: Object
  ) {
    this.criteriaFormGroup = new FormGroup({
      breakpoint70: this.breakpoint70FormControl,
      equipLoad: this.equipLoadFormControl,
      fire: new FormControl(1.0, [Validators.required]),
      holy: new FormControl(1.0, [Validators.required]),
      light: new FormControl(1.0, [Validators.required]),
      magic: new FormControl(1.0, [Validators.required]),
      phy: new FormControl(1.0, [Validators.required]),
      'phy.pierce': new FormControl(1.0, [Validators.required]),
      'phy.slash': new FormControl(1.0, [Validators.required]),
      'phy.strike': new FormControl(1.0, [Validators.required]),
      poise: new FormControl(1.0, [Validators.required]),
      'res.focus': new FormControl(1.0, [Validators.required]),
      'res.immunity': new FormControl(1.0, [Validators.required]),
      'res.robustness': new FormControl(1.0, [Validators.required]),
      'res.vitality': new FormControl(1.0, [Validators.required]),
    });
    this.breakpoint70FormControl.disable();
    this.onEquipLoadChange();
  }

  // Form Components
  @ViewChild('armorSelection') armorSelection!: ElementRef<HTMLDivElement>;
  form!: FormGroup;
  criteriaFormGroup: FormGroup;
  breakpoint70FormControl = new FormControl(0);
  // enduranceFormControl = new FormControl(10, [
  //   Validators.required,
  //   Validators.min(1),
  //   Validators.max(99),
  // ]);
  equipLoadFormControl = new FormControl(48.2, [Validators.required]);

  // Dataset
  armorSets?: ArmorSets;

  // Rendering / Computational data objects
  get attrRange(): ReadonlyArray<number> {
    return Array(100)
      .fill(0)
      .map((_, idx) => 1 + idx);
  }

  ngOnInit(): void {
    console.log('OptimizerComponent', 'ngOnInit');
    this.route.data.subscribe((data) => {
      console.log('OptimizerComponent', 'data.subscribe');
      this.armorSets = data['armorSets'];
      console.log('OptimizerComponent', 'data.subscribe done', this.armorSets);

      const groups: any = {};

      this.armorSets!.forEach((value, key) => {
        const set_controls: SetControlType = new SetControls();
        if (value.Helm) {
          set_controls.Helm = new FormControl(true);
        }
        if (value['Chest Armor']) {
          set_controls['Chest Armor'] = new FormControl(true);
        }
        if (value.Gauntlets) {
          set_controls.Gauntlets = new FormControl(true);
        }
        if (value['Leg Armor']) {
          set_controls['Leg Armor'] = new FormControl(true);
        }
        groups[key.toString()] = new FormGroup(set_controls);
      });

      this.form = new FormGroup(groups);
    });

    if (isPlatformBrowser(this.platformId)) {
      import('bootstrap').then((bootstrap) => {
        const help = new bootstrap.Offcanvas(this.armorSelection.nativeElement);
        help.show();
      });
    }
  }

  ngAfterViewInit(): void {
    console.log('OptimizerComponent', 'ngAfterViewInit');
  }

  armorSetFormGroup(armorSetKey: string | String): FormGroup {
    const rv = this.form.controls[armorSetKey.toString()];
    if (rv) {
      return rv as FormGroup;
    } else {
      throw new Error(
        "IllegalState: form['" + armorSetKey + "'] should have exited"
      );
    }
  }

  checkAllSelected(armorSetKey: string | String): boolean {
    const controls = this.itemCheckboxControls(armorSetKey);
    return controls.every((control) => control.value);
  }

  checkSomeSelected(armorSetKey: string | String): boolean {
    const controls = this.itemCheckboxControls(armorSetKey);
    const numSelected = controls.filter((control) => control.value).length;
    return 0 < numSelected && numSelected < controls.length;
  }

  itemCheckboxControls(armorSetKey: string | String): AbstractControl[] {
    const group = this.armorSetFormGroup(armorSetKey);
    const controls = Object.keys(group.controls)
      .filter((key) => key !== 'all')
      .map((controlKey) => group.controls[controlKey]);
    return controls;
  }

  onEquipLoadChange() {
    this.breakpoint70FormControl.setValue(
      Math.floor(70 * this.equipLoadFormControl.value) / 100
    );
    console.log('onEquipLoadChange', event, this.equipLoadFormControl.value);
  }

  requireKeypressNumber(event: KeyboardEvent): boolean {
    const isDigit = /\d/.test(event.key);
    if (!isDigit) {
      event.preventDefault();
    }
    return isDigit;
  }

  selectAll(armorSetKey: string | String, checked: boolean): void {
    this.itemCheckboxControls(armorSetKey).forEach((control) =>
      control.setValue(checked)
    );
  }
}
